USE master
GO

DROP DATABASE playground_appliances
GO

CREATE DATABASE playground_appliances
GO

USE playground_appliances
GO

CREATE TABLE Appliances (
ID char(3) PRIMARY KEY,
AppType varchar(20), 
StoreID char(2),
Cost real,
Price real)

CREATE TABLE Stores (
StoreID char(2) PRIMARY KEY,
City varchar(30) NOT NULL,
Rating int)

CREATE TABLE Salespeople (
EmployeeID char(4) PRIMARY KEY,
EmployeeName varchar(30) NOT NULL,
CommRate int,
BaseSalary int,
SupervisorID char(4))

CREATE TABLE Sales (
SaleDate date,
EmployeeID char(4),
AppID char(3),
Qty int)

insert into Salespeople values ('1235', 'Linda Smith', 15, 1200, '1412')
insert into Salespeople values ('1412', 'Anne Green', 12, 1800, NULL)
insert into Salespeople values ('2920', 'Charles Brown', 10, 1150, '1412')
insert into Salespeople values ('3231', 'Harry Purple', 18, 1700, '1412')

insert into Stores values ('22', 'Hamilton', 8)
insert into Stores values ('20', 'Te Awamutu', 6)
insert into Stores values ('27', 'Huntly', 9)

insert into Appliances values ('100', 'Refrigerator', '22', 150, 250)
insert into Appliances values ('150', 'Television', '27', 225, 340)
insert into Appliances values ('110', 'Refrigerator', '20', 175, 300)
insert into Appliances values ('200', 'Microwave Oven', '22', 120, 180)
insert into Appliances values ('300', 'Washer', '27', 200, 325)
insert into Appliances values ('310', 'Washer', '22', 280, 400)
insert into Appliances values ('400', 'Dryer', '20', 150, 220)
insert into Appliances values ('420', 'Dryer', '22', 240, 360)

insert into Sales values ('2010/1/1','1412','150',1)
insert into Sales values ('2010/1/5','3231','110',1)
insert into Sales values ('2010/1/3','2920','110',2)
insert into Sales values ('2010/1/13','1412','100',1)
insert into Sales values ('2010/1/25','1235','150',2)
insert into Sales values ('2010/1/22','1235','100',2)
insert into Sales values ('2010/1/12','2920','150',3)
insert into Sales values ('2010/1/14','3231','100',1)
insert into Sales values ('2010/1/15','1235','300',1)
insert into Sales values ('2010/1/3','2920','200',2)
insert into Sales values ('2010/1/31','2920','310',1)
insert into Sales values ('2010/1/5','1412','420',1)
insert into Sales values ('2010/1/15','3231','400',2)
GO

-- Create a view called Sales_by_Month showing the sales per salesperson for the month.
-- OUTPUT:
-- Anne Green 3
-- Charles Brown 8
-- Harry Purple 4
-- Linda Smith 5
CREATE VIEW [Sales by Month]
AS
SELECT EmployeeName, SUM(Qty) AS [Total Sales this month]
FROM Salespeople P, Sales S
WHERE P.EmployeeID = S.EmployeeID
GROUP BY EmployeeName
GO

SELECT * FROM [Sales by Month]
GO
--Create a view called Fridge_Sales_by_Branch showing the sales per branch for the month. 
--OUTPUT:
--20 3
--22 4
CREATE VIEW [Fridge Sales by Branch]
AS
SELECT StoreID, SUM(Qty) AS [Total Fridge Sales]
FROM Appliances A, Sales S
WHERE A.ID = S.AppID
AND A.AppType LIKE 'Refrigerator'
GROUP BY StoreID
GO

Select * FROM [Fridge Sales by Branch]
GO

--Create a view called Sales_View giving the names of salespeople getting more than 10% commission, together with the types of appliances they have sold.
--OUTPUT:
-- Anne Green Dryer
-- Anne Green Refrigerator
-- Anne Green Television
-- Harry Purple Dryer
-- Harry Purple Refrigerator
-- Linda Smith Refrigerator
-- Linda Smith Television
-- Linda Smith Washer
CREATE VIEW [Sales View]
AS
SELECT DISTINCT P.EmployeeName, A.AppType
FROM Salespeople P, Appliances A, Sales S
WHERE P.EmployeeID = S.EmployeeID
AND S.AppID = A.ID
AND P.CommRate > 10
GO

SELECT * FROM [Sales View]
GO

--Write a stored procedure that displays a table of appliance sales by branch for a given appliance, for example:
--OUTPUT:
--StoreID Number of Sales
--	20			3
--	22			4
CREATE PROCEDURE [Sale of AppType at Branch]
@appliance VARCHAR(20)
AS
SELECT ST.StoreID, SUM(Qty) AS [Number of Sales]
FROM Stores ST, Sales S, Appliances A
WHERE ST.StoreID = A.StoreID
AND A.ID = S.AppID
AND A.AppType LIKE @appliance
GROUP BY ST.StoreID
GO

EXECUTE [Sale of AppType at Branch] 'refrigerator'
GO

--Write a stored procedure that returns the number of sales of a given appliance for the month, for example, when testing your procedure using:
-- declare @n int
-- EXEC NumSales 'Dryer', @n output
-- PRINT 'The number of sales of Dryer is' + STR(@n,2)
--OUTPUT: The number of sales of Dryer is 3
--DROP PROCEDURE [Appliance Sales]
CREATE PROCEDURE [Appliance Sales]
@appliance VARCHAR(20),
@num INT OUTPUT
AS
SELECT SUM(Qty) AS [Total Sales]
FROM Sales S, Appliances A
WHERE S.AppID = A.ID
AND A.AppType LIKE @appliance
GO

DECLARE @num INT
EXECUTE [Appliance Sales] 'dryer', @num OUTPUT
PRINT 'The number of sales of Dryer is' + STR(@num, 2)
GO
--I'll need to get some help with this

--Write a stored procedure to display the profitability of each of the salespeople ordered from the most profitable to the least.
--OUTPUT:
--		Name	Gross Commission Net Profit
-- Charles Brown 2380	83.5		751.5
-- Linda Smith	 1505	83.25		471.75
-- Harry Purple  990	65.7		299.3
-- Anne Green    950	40.2		294.8
SELECT P.EmployeeName,
SUM((S.Qty)*A.Price) AS Gross,
SUM(S.Qty*(A.Price - A.Cost)) AS Commission,
SUM((A.Price - A.Cost)) AS Profit
FROM Salespeople P, Sales S, Appliances A
WHERE P.EmployeeID = S.EmployeeID
AND S.AppID = A.ID
GROUP BY P.EmployeeName
ORDER BY Gross DESC
--I'll need some help with this too
GO

CREATE TRIGGER [No Date Change]
ON Sales
FOR UPDATE AS 
	IF UPDATE (SaleDate) BEGIN
		RAISERROR('You cannot change the sale date', 16, 1)
		ROLLBACK TRANSACTION
	END
GO

--Write safe tests for your trigger in Q1, one to test a single insert and the other to test multiple update. You should get messages like the following:
--OUTPUT:
-- Msg 50000, Level 16, State 1, Procedure noDateChange, Line 5
-- Cannot make changes to the Date field of the Sales table
-- Msg 3609, Level 16, State 1, Line 2
-- The transaction ended in the trigger. The batch has been aborted.
-- and also:
-- Msg 50000, Level 16, State 1, Procedure noDateChange, Line 5
-- Cannot make changes to the Date field of the Sales table
-- Msg 3609, Level 16, State 1, Line 2
-- The transaction ended in the trigger. The batch has been aborted.
UPDATE Sales
SET SaleDate = '2010/1/5'
WHERE AppID = 150
GO
UPDATE Sales
SET SaleDate = '2010/1/7'
WHERE AppID = 150
GO

--