USE master
GO

DROP DATABASE playground_buildings
GO

CREATE DATABASE playground_buildings
GO

USE playground_buildings
GO

CREATE TABLE Employees (
EmployeeID char(4) PRIMARY KEY,
EmployeeName varchar(30) NOT NULL,
HourlyRate real,
SkillType varchar(20),
SupervisorID char(4) NOT NULL)

CREATE TABLE Building (
BuildingID char(3) PRIMARY KEY,
BuildingAddress varchar(20), 
BuildingType varchar(10),
QualityLevel int)

CREATE TABLE Job (
EmployeeID char(4),
BuildingID char(3),
NumDays int,
FOREIGN KEY (EmployeeID) REFERENCES Employees (EmployeeID),
FOREIGN KEY (BuildingID) REFERENCES Building (BuildingID))

insert into Employees values ('1235', 'J. Smith', 12.5, 'electrical', '1311')
insert into Employees values ('1412', 'A. Green', 13.75, 'plumbing', '1520')
insert into Employees values ('2920', 'J. Brown', 10.0, 'roofing', '2920')
insert into Employees values ('3231', 'A. Purple', 17.4, 'carpenter', '3231')
insert into Employees values ('1520', 'J. Red', 11.75, 'plumbing', '1520')
insert into Employees values ('1311', 'A. Orange', 15.5, 'electrical', '1311')
insert into Employees values ('3001', 'A. Black', 8.2, 'carpenter', '3231')

insert into Building values ('312', '123 Tree Rd', 'office', 2)
insert into Building values ('435', '456 Ash Rd', 'retail', 1)
insert into Building values ('515', '789 Beech Rd', 'residence', 3)
insert into Building values ('210', '1234 Sycamore Rd', 'office', 3)
insert into Building values ('111', '1235 Plum Rd', 'office', 4)
insert into Building values ('460', '1236 Oak Rd', 'warehouse', 3)

insert into Job values ('1235','312',5)
insert into Job values ('1412','312',10)
insert into Job values ('1235','515',22)
insert into Job values ('2920','460',18)
insert into Job values ('1412','460',18)
insert into Job values ('2920','435',10)
insert into Job values ('2920','210',15)
insert into Job values ('3231','111',8)
insert into Job values ('1412','435',15)
insert into Job values ('1412','515',8)
insert into Job values ('3231','312',20)
insert into Job values ('1520','515',14)
insert into Job values ('1311','435',12)
insert into Job values ('1412','210',12)
insert into Job values ('1412','111',4)
insert into Job values ('3001','111',14)
insert into Job values ('1311','460',24)
insert into Job values ('1520','312',17)
insert into Job values ('3001','210',14)
GO

--Create a view called Current_Projects showing current projects. This view should only display the address and should be grouped by the type of the building.
--OUTPUT:
-- 123 Tree Rd office
-- 1234 Sycamore Rd office
-- 1235 Plum Rd office
-- 1236 Oak Rd warehouse
-- 456 Ash Rd retail
-- 789 Beech Rd residence
CREATE VIEW [Current Projects]
AS
SELECT BuildingAddress, BuildingType
FROM Building
GO

SELECT *
FROM [Current Projects]
ORDER BY BuildingAddress
GO

--Create a view called High_Earner that shows all details about employees who earn more than $12 an hour.
--OUTPUT:
-- 1235 J. Smith 12.5 electrical 1311
-- 1311 A. Orange 15.5 electrical 1311
-- 1412 A. Green 13.75 plumbing 1520
-- 3231 A. Purple 17.4 carpenter 3231
CREATE VIEW [High Earner]
AS
SELECT *
FROM Employees E
WHERE E.HourlyRate > 12
GO

SELECT * FROM [High Earner]
GO

--Insert a new employee into the database using the High_Earner view with values (1122, 'J. Mauve', 11.5, 'carpenter', 3231)
--Demonstrate that the data has been inserted using a normal select statement on the Employee table:
--OUTPUT:
-- 1235 J. Smith 12.5 electrical 1311
-- 1412 A. Green 13.75 plumbing 1520
-- 2920 J. Brown 10 roofing 2920
-- 3001 A. Black 8.2 carpenter 3231
-- 1520 J. Red 11.75 plumbing 1520
-- 1311 A. Orange 15.5 electrical 1311
-- 3231 A. Purple 17.4 carpenter 3231
-- 1122 J. Mauve 11.5 carpenter 3231
INSERT INTO [High Earner]
SELECT 1122, 'J. Mauve', 11.5, 'carpenter', 3231
GO

SELECT * FROM Employees
GO

--Drop the view from Q5 and delete the row inserted by Q6. Now write a new view that would not allow the insert from Q6 to take place. Then attempt the insert statement
--from Q6 again and the attempted insert should give something like:
-- Msg 550, Level 16, State 1, Line 1
-- The attempted insert or update failed because the target
-- view either specifies WITH CHECK OPTION or spans a view
-- that specifies WITH CHECK OPTION and one or more rows
-- resulting from the operation did not qualify under the
-- CHECK OPTION constraint.
-- The statement has been terminated.
DROP VIEW [High Earner]
GO

CREATE VIEW [High Earner]
AS
SELECT *
FROM Employees E
WHERE E.HourlyRate > 12
AND E.EmployeeName NOT LIKE 'J. Mauve'
WITH CHECK OPTION
GO

SELECT * FROM [High Earner]
GO

--This works but break the build, so I've commented it out
--INSERT INTO [High Earner]
--SELECT 9999, 'J. Mauve', 11.5, 'carpenter', 3231
--GO
CREATE PROCEDURE [Check Rates]
@rate INT
AS
SELECT * 
FROM Employees E
WHERE E.HourlyRate > @rate
GO

EXECUTE [Check Rates] 11.5
GO
--Ok, so why is this happening? Why is it showing the value for an hourly rate which equals the input?

--Write a stored procedure that returns the average hourly rate of employees of a given type, for example:
CREATE PROCEDURE [avgRate]
@skill VARCHAR(25),
@rate REAL OUTPUT
AS
SELECT AVG(E.HourlyRate) AS [Averge Rate]
FROM Employees E
WHERE E.SkillType LIKE @skill
GO

DECLARE @rate REAL
EXECUTE [avgRate] 'plumbing', @rate OUTPUT
PRINT 'The average rate of plumbers is' + CONVERT(CHAR(10), @rate)
GO
--This is the second ocurrence of the output problem

